import processing.serial.*; // Importing the serial library to communicate with the Arduino
Serial myPort; // Initializing a vairable named 'myPort' for serial communication
int cor;
int cor1;
String arduino;
float x;
float y;
float miraX;
float miraY;
float estado;
float ultraX;
float ultraY;
float teta;
void setup ( ) {
size (500, 500); // Size of the serial window, you can increase or decrease as you want
background ( 1000 );
myPort = new Serial (this, "COM9", 9600); // Set the com port and the baud rate according to the Arduino
myPort.bufferUntil ( '\n' ); // Receiving the data from the Arduino IDE
}
void serialEvent (Serial myPort) {
arduino = myPort.readStringUntil ( '\n' ) ; // Changing the background color according to received data
float p[] = float(split(arduino, ','));
x=p[0]*0.51836278784; //40*x=6,6pi // x=6,6pi/40 0.51836278784
y=p[1]*0.51836278784;
teta=p[2];
estado=p[3];
miraX=x+10*cos(teta);
miraY=y+10*sin(teta);
ultraX=miraX+estado*cos(teta);
ultraY=miraY+estado*sin(teta);
}
void apagar()
{
float tamanho;
color white = color(255);
color red = color(#ff0000);
color black = color(0);
color verde = color(#00ff00);
color atual;
tamanho = 1000;
 for (int i=0; i<tamanho; i++)
 {
 for (int j=0; j<tamanho; j++)
 {
 atual = get(i,j);
 if (atual == red) {set(i,j, white);}
 if (atual == verde) {set(i,j, black);}
 }
 }
 }
void draw ( ) {
apagar();
smooth();
fill(255);
106
stroke(#ff0000);
rectMode(CENTER);
rect(x,y,15,15);
if(estado<70)
{cor=0;
cor1=#00ff00;}
else
{cor=255;
cor1=#ff0000;}
fill(#ff0000); // preenchimento da mira
strokeWeight(2);
stroke(255);
line(miraX,miraY,ultraX,ultraY);
strokeWeight(1);
rectMode(CENTER);
rect(miraX,miraY,5,5);
fill(cor);
stroke(cor1);//
rectMode(CENTER);
rect(ultraX,ultraY,2,2);
print(x);
print("..");
print(y);
print("..");
println(estado);
////////////////estado de click
if ( mousePressed && ( mouseButton == RIGHT ) ) { // if the right mouse button is pressed
myPort.write ( '5' ) ; // Send a '0' to the Arduino IDE
}
}
void keyPressed(){
if(key==CODED){
if(keyCode ==UP){ myPort.write ( '0' ) ;
}
if(keyCode==DOWN){myPort.write ( '1' ) ;
}
if(keyCode==LEFT){myPort.write ( '2' ) ;
}
if(keyCode==RIGHT){myPort.write ( '3' ) ;
}
if(keyCode==ALT){myPort.write ( '4' ) ;
} } }
