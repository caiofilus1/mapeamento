#include <AFMotor.h>
AF_DCMotor motor1(4); //Seleção do Motor 1
AF_DCMotor motor2(3); //Seleção do Motor 2
#define trig A4 //Saída para o pino de trigger do sensor
#define echo A5 //Saída para o pino echo do sensor
int a; //Variavel qualquer para entrar em modo automático
int Auto; //Variavel que determina se está em modo automático ou manual
float measureDistance(); //Função para medir, calcular e retornar a distância em cm
void trigPulse(); //Função que gera o pulso de trigger de 10µs
void decision(); //Função para tomada de decisão. Qual melhor caminho?
void ContarAnguloPos();
void robot_forward(); //Função para movimentar robô para frente
void robot_backward(); //Função para movimentar robô para trás
void robot_left(); //Função para movimentar robô para esquerda
void robot_right(); //Função para movimentar robô para direita
void robot_stop(); //Função para parar o robô
float dist_cm; //Armazena a distância em centímetros entre o robô e o obstáculo
float angulo; //Armazena angulo em radiandos do giro do robô
int MaxX; //Armazena a distância X maxima no mapa em centimetros
int MaxY; //Armazena a distância Y maxima no mapa em centimetros
int MaxPixel; //Armazena a largura cm do mapa grafico considerando uma matriz quadrada
float contadorNew; //Contador usado para giro durante o modo automático
float contadorX; //Contador usado para calculo de distância X
float contadorY; //Contador usado para calculo de distância Y
int contadorr; //Contador usado para calculo da angulação em radianos
String mark; //Marca o sentido do robô
void setup() {
 // put your setup code here, to run once:
pinMode(trig, OUTPUT); //Saída para o pulso de trigger
pinMode(echo, INPUT); //Entrada para o pulso de echo
digitalWrite(trig, LOW); //Pino de trigger inicia em low
delay(500);
Serial.begin(9600); // Starting the serial communication at 9600 baud rate
// = 0xFF; //Inicia no valor máximo 0xFF
pinMode(2, INPUT); // Primeiro pino de interrupção
attachInterrupt(0, ContarAnguloPos, RISING); //
angulo=0; //Angulo inicial
contadorX=50; //Começa em uma posição X determinada no mapa
contadorY=50; //Começa em uma posição Y determinada no mapa
Auto=0; //Começa em modo manual (0) ou automático (5)
a=1; // 1 para entrar no if usado no modo automatico
MaxPixel=500; //Dimensão máxima do mapa da matriz em cm
MaxX=MaxY=2*(MaxPixel-15);
 }
void loop() {
//robot_forward(); //Usado para teste
//robot_left(); //Usado para teste
//robot_backward();//Usado para teste
//robot_right(); //Usado para teste
if (Serial.available ( ) > 0) { // Checking if the Processing IDE has send a value or not
char state = Serial.read ( ); // Reading the data received and saving in the state variable
if(state == '0')
{
Auto=0;
robot_forward();
102
}
if (state == '1') {
Auto=0;
robot_backward();
}
if (state == '2') {
Auto=0;
robot_left();
}
if (state == '3') {
Auto=0;
robot_right();
}
if (state == '4') {
Auto=0;
robot_stop();
}
if (state== '5'){
Auto=5;
 }
 }
dist_cm=measureDistance();
if(Auto==5){
 robot_forward();
if((dist_cm<20) || (contadorX>=MaxX) || (contadorY>=MaxY) || (contadorX<=0) || (contadorY<=0) )
decision();
 }
//Serial.println((String)contadorX+","+contadorY+","+anguloA+",,,,,,,,,,,,,,,,,,,"+anguloB); // //Usado para teste
// Serial.println((String)contadorX+","+contadorY+","+dist_cm+",,,,,,,,,yyyy,,,,,,,,"+contadorr); // para teste
Serial.println((String)contadorX+","+contadorY+","+angulo+","+dist_cm);//Deve ir para porta serial
}
float measureDistance() //Função que retorna a distância em centímetros
{
 float pulse; //Armazena o valor de tempo em µs que o pino echo fica em nível alto
 float H; //Limitador do ultrassom;
 trigPulse(); //Envia pulso de 10µs para o pino de trigger do sensor
 pulse = pulseIn(echo, HIGH); //Mede o tempo em que echo fica em nível alto e armazena na variável pulse
 H= pulse/58.82; //Tempo que leva para a onda sonora ir e voltar
 if(H>=70) //Limitado um valor máximo de 70cm;
 H=70;
 return (H); //Retorna o valor
} //end measureDistante
void trigPulse() //Função para gerar o pulso de trigger para o sensor HC-SR04
{
 digitalWrite(trig,HIGH); //Saída de trigger em nível alto
 delayMicroseconds(10); //Por 10µs ...
 digitalWrite(trig,LOW); //Saída de trigger volta a nível baixo
} //end trigPulse
void decision()
{
 contadorNew=0;
 if (a==1) // Foi colocado para teste
 {
 do { robot_right();
 dist_cm = measureDistance();
 // Serial.println((String)contadorX+","+contadorY+","+dist_cm+",,,,,,,,"+contadorr);// Usado para teste
 Serial.println((String)contadorX+","+contadorY+","+angulo+","+dist_cm); // Deve ir para porta serial, para
que processing receba;
// Serial.println((String)contadorX+","+anguloB+","+contadorNew+",,,,,,,,,,,,,,,,,,,"+contadorr);// Usado teste
103
 }while(contadorNew<16); //Vai pra direita enquanto o contador de giro não alcança 19
 robot_forward();
 } //end if
 } //end decision
void ContarAnguloPos(){ // Função para valor de Angulo e Posição
 static unsigned long delayest; // delay falso para retornar um estado sem "tremida"
 if ( mark=="direita") {
if (millis()-delayest>1){
contadorr= contadorr+1;
angulo=contadorr*3.1415/39;
if(Auto==5)
contadorNew=contadorNew+1;
else
contadorNew=0;
if(angulo>=6.283)
contadorr=0;
}
delayest=millis();
 } // end mark direita
if (mark=="esquerda") {
if (millis()-delayest>1){
contadorr= contadorr-1;
angulo=contadorr*3.1415/39;
if(Auto==5)
contadorNew=contadorNew+1;
else
contadorNew=0;
if(angulo<=-6.283)
contadorr=0;
}
delayest=millis();
 } // end mark esquerda
if (mark=="re") {
if (millis()-delayest>1){
contadorX= contadorX-1*cos(angulo);
contadorY= contadorY-1*sin(angulo);
if(contadorX>MaxX)
{robot_stop();
contadorX=MaxX;}
if(contadorX<0)
{robot_stop();
contadorX=0;}
if(contadorY>MaxY)
{robot_stop();
contadorY=MaxY;}
if(contadorY<0)
{robot_stop();
contadorY=0;}
}
delayest=millis();
 } // end mark re
if (mark=="frente") {
if (millis()-delayest>1){
contadorX= contadorX+1*cos(angulo);
contadorY= contadorY+1*sin(angulo);
if(contadorX>MaxX)
{robot_stop();
contadorX=MaxX;}
if(contadorX<0)
{robot_stop();
104
contadorX=0;}
if(contadorY>MaxY)
{robot_stop();
contadorY=MaxY;}
if(contadorY<0)
{robot_stop();
contadorY=0;} }
delayest=millis();
 } //end mark frente

}
void robot_forward()
{ mark="frente";
 motor1.setSpeed(180);
 motor1.run(FORWARD);
 motor2.setSpeed(215);
 motor2.run(FORWARD);
 } //end robot forward
void robot_backward()
{
mark="re";
 motor1.setSpeed(180);
 motor1.run(BACKWARD);
 motor2.setSpeed(215);
 motor2.run(BACKWARD);
 } //end robot backward
void robot_left()
{ mark="esquerda";
 motor1.setSpeed(130);
 motor1.run(BACKWARD);
 motor2.setSpeed(165);
 motor2.run(FORWARD);
 } //end robot left
void robot_right()
{ mark="direita";
 motor1.setSpeed(130);
 motor1.run(FORWARD);
 motor2.setSpeed(165);
 motor2.run(BACKWARD)
;
 } //end robot right
void robot_stop()
{ // sem mark
 motor1.setSpeed(0);
 motor1.run(RELEASE);
 motor2.setSpeed(0);
 motor2.run(RELEASE);
 } //end robot stop
